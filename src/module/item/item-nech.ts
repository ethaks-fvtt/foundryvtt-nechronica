import NechronicaActor from "../actor/actor-nech";
import { NECHRONICA, RollType } from "../config";
import { Modifier, NechronicaItemData, PartEffect } from "./item-data";
import { NechronicaRollData } from "../common/common-data";
import { getGame, localize } from "../lib/utils";

/*
 * Nechronica's Item entity
 */
export default class NechronicaItem extends Item {
  /**
   * A modifier with default properties
   */
  static get defaultModifier(): Modifier {
    return {
      formula: "",
      optional: 0,
      target: "",
      type: "",
      tag: "",
    };
  }

  /**
   * A convenience getter to check for repeatable parts
   */
  get isRepeatable(): boolean {
    if (this.data.type === "bodyPart") {
      return ["action", "auto"].includes(this.data.data.timing);
    }
    return true;
  }

  /**
   * A convenience getter to check whether a part should be considered broken.
   * Doll skills are always considered intact, all other parts depend on their data state.
   */
  get isBroken(): boolean {
    if (this.data.type !== "bodyPart") return false;
    const actorType = (this.actor as unknown as NechronicaActor)?.data.type;
    const partType = this.data.data.type;

    if (actorType === "doll" && partType === "skill") return false;
    return this.data.data.broken;
  }

  /**
   * Gets the chat data belonging to an item
   */
  public getChatData(): ItemChatData {
    const itemData = this.data;
    const rollData = this.getRollData();
    const result: ItemChatData = {
      effect: this.getEffectData(rollData),
      description: this.getDescriptionData(rollData),
      props: [],
      extraProps: [],
    };

    result.effect = this.getEffectData(rollData);
    result.description = this.getDescriptionData(rollData);

    // Body Part specific data
    if (itemData.type === "bodyPart") {
      if (NECHRONICA.attackTypes[itemData.data.attack]) {
        result.props.push(NECHRONICA.attackTypes[itemData.data.attack]);
      }
      if (itemData.data.type === "bodyPart" && NECHRONICA.locations[itemData.data.location]) {
        result.props.push(NECHRONICA.locations[itemData.data.location]);
      }
      if (NECHRONICA.timings[itemData.data.timing]) {
        result.props.push(NECHRONICA.timings[itemData.data.timing]);
      }
      if (itemData.data.range) {
        result.props.push(localize("Range") + ` ${itemData.data.range}`);
      }
      if (itemData.data.cost !== undefined) {
        result.props.push(localize("Cost") + ` ${itemData.data.cost} ` + localize("AP"));
      }

      for (const [effect, state] of Object.entries(itemData.data.effects)) {
        if (state) {
          result.props.push(NECHRONICA.effectTypes[effect as PartEffect]);
        }
      }
    } else if (itemData.type === "fetter") {
      // Fetter specific data
      result.props.push(localize("Target") + ` ${itemData.data.target}`);
      if (itemData.data.type) {
        result.props.push(localize("Type") + ` ${itemData.data.type}`);
      }
      result.props.push(localize("Madness") + ` ${itemData.data.madness}`);
    }

    return result;
  }

  /**
   * Gets the item's enriched effect text
   *
   * @param rollData - Roll data to enrich the text with
   * @returns Enriched effect text
   */
  getEffectData(rollData: NechronicaRollData): string {
    rollData = rollData ?? this.getRollData();
    const effect = getProperty(this.data, "data.effect") ?? "";
    const result = TextEditor.enrichHTML(effect, { rollData });
    return result;
  }

  /**
   * Gets the item's enriched description text
   *
   * @param rollData - Roll data to enrich the text with
   * @returns Enriched description text
   */
  getDescriptionData(rollData: NechronicaRollData): string {
    rollData = rollData ?? this.getRollData();
    const description = getProperty(this.data, "data.description") ?? "";
    const result = TextEditor.enrichHTML(description, { rollData });
    return result;
  }

  /** @override */
  // @ts-expect-error Breaks inheritance, but only the system and its users consume this data
  getRollData(): NechronicaRollData {
    return {
      ...(this.actor?.getRollData() ?? {}),
      item: foundry.utils.deepClone(this.data.data),
      itemType: this.data.type,
    } as NechronicaRollData;
  }

  /** @override */
  prepareData(): void {
    super.prepareData();
    // Add location dependent image to body parts
    if (
      this.data.type === "bodyPart" &&
      this.data._source.img === foundry.data.ItemData.DEFAULT_ICON
    ) {
      // Fallback icon for skills
      if (this.data.data.type === "skill")
        this.data.img = "/icons/skills/trades/academics-investigation-puzzles.webp";

      // Fallback icon for bodyParts
      if (this.data.data.type === "bodyPart" && this.data.data.location)
        this.data.img = `/systems/nechronica/icons/parts_${this.data.data.location}.png`;
    }
  }

  /**
   * Rolls an Item to chat, optionally including dice rolls for checks or attacks
   *
   * @param rollType - The rollType (simple chat, check, attack)
   * @param dice - The number of d10 used for checks
   * @param skipDialog - Whether the dialog should explicitly get skipped
   * @param rollBonus - Roll bonus applied in addition to any other bonuses
   * @param damageBonus - Damage bonus applied in addition to any other bonuses
   * @returns The chat message created
   */
  async roll({
    rollType = 0,
    dice = 1,
    skipDialog = false,
    rollBonus = 0,
    damageBonus = 0,
  }: {
    rollType?: RollType;
    dice?: number;
    skipDialog?: boolean;
    rollBonus?: number;
    damageBonus?: number;
  } = {}): Promise<ChatMessage | void | null> {
    const actor = this.actor != null && this.actor instanceof NechronicaActor ? this.actor : null;
    if (actor === null) return;
    const token = actor?.token;
    const itemData = this.data;
    const template = "systems/nechronica/templates/chat/item-card.hbs";
    const rollData = this.getRollData();
    skipDialog = getGame().keyboard?.isDown("Shift") ?? false;
    const isAttack = rollType === RollType.ATTACK;
    const isCheck = rollType === RollType.CHECK;
    const hasAttack = "attack" in itemData.data && !!itemData.data.attack;
    let roll, damageRoll, form: JQuery<HTMLElement>; //>
    const combatant = actor?.combatant;

    // Template data collection to be used for dialog and chat message
    const templateData: TemplateData = {
      actor,
      tokenId: token ? `${token.object?.scene.id}.${token.id}` : undefined,
      item: this.data,
      rollMode: getGame().settings.get("core", "rollMode") as keyof typeof CONFIG.Dice.rollModes,
      rollModes: CONFIG.Dice.rollModes,
      config: NECHRONICA,
      data: this.getChatData(),
      isAttack,
      isCheck,
      hasAttack,
      spineCounter: this.actor?.spineCounter ?? 0,
      roll: undefined,
      // Dedupe optional modifiers (for easier display), collect under same label
      optionalModifiers: actor?.optionalModifiers
        ?.filter(
          (o) =>
            ("attack" in itemData.data && o.modifier.target === itemData.data.attack) ||
            (isAttack && o.modifier.type === "damage")
        )
        .reduce((acc: { name: string; modifiers: Modifier[] }[], val) => {
          if (!acc.find((o) => o.name === val.name)) {
            acc.push({ name: val.name, modifiers: [val.modifier] });
          } else {
            acc.find((o) => o.name === val.name)?.modifiers.push(val.modifier);
          }
          return acc;
        }, []),
      dice,
      result: { roll: "", type: "", result: "", location: "" },
    };

    // Handle rolls
    if (itemData.type === "bodyPart" && (isAttack || isCheck)) {
      // Combat handling
      const cost = itemData.data.cost;
      let ap;

      if (combatant && isAttack) {
        ap = actor?.ap;

        // Zero or less AP
        if (ap != null && ap < 1) {
          ui.notifications?.warn(
            localize("Warnings.MissingAP", {
              actor: this.actor?.name,
              part: this.name,
            })
          );
          return;
        }

        // Used up attack or part
        if (itemData.data.used && !this.isRepeatable && !getGame().keyboard?.isDown("Control")) {
          ui.notifications?.warn(
            localize("Warnings.AlreadyUsed", {
              actor: this.actor?.name,
              part: this.name,
            })
          );
          return;
        }
      }

      // Show dialog when not skipping
      if (!skipDialog) {
        // Dialog data
        const dialogContent = await renderTemplate(
          "systems/nechronica/templates/roll-dialog.hbs",
          templateData
        );
        // Await dialog response
        form = await new Promise((resolve) => {
          new Dialog({
            title: localize(`${isAttack ? "Attack" : "Check"}`) + `: ${this.name}`,
            content: dialogContent,
            buttons: {
              roll: {
                label: localize("Roll.Roll"),
                callback: (html) => resolve(html as JQuery<HTMLElement>),
              },
            },
            default: "roll",
            // close: () => reject(new Error("Roll was cancelled")),
          }).render(true);
        });

        // Cancel roll on dialog closing
        if (form === undefined) return;
      }
      // This assertion should not be necessary, TS doesn't know about resolved Promises though
      /* eslint-disable @typescript-eslint/no-non-null-assertion */
      form = form! as JQuery<HTMLElement>;

      if (combatant && isAttack) {
        // Set used status
        if (!this.isRepeatable) await this.update({ "data.used": true });
        // Decrease AP
        const useSpineCounter: boolean =
          form?.find('[name="spine-counter"]').prop("checked") ?? true;
        actor?.spendAp(cost, { useSpineCounter });
      }

      if ((isAttack && hasAttack) || isCheck) {
        // Actual Roll creation
        const parts: Array<string | number> = [];
        const damageParts: Array<string | number> = [];
        // Get number of dice, default to parameter
        const nd = Number(form?.find('[name="dice"]:checked').val() ?? dice);
        parts.push(`${nd}d10${nd > 1 ? "kh" : ""}`); // `
        // TODO: What did I ever mean with this? Investigate!
        // if (rollData.item.modifier) parts.push("@modifier"); // `

        // General modifier
        if (isAttack && getProperty(rollData, `modifiers.${itemData.data.attack}.attack`)) {
          parts.push(`@modifiers.${itemData.data.attack}.attack`);
          templateData.data.extraProps.push(
            ...(actor?.sourceDetails?.[`data.modifiers.${itemData.data.attack}.attack`]?.map(
              (m) => m.name
            ) ?? [])
          );
        }

        // Roll bonus from form
        rollData.rollBonus = Number(form?.find('[name="roll-bonus"]').val()) + rollBonus;
        if (rollData.rollBonus) parts.push("@rollBonus");

        // Damage bonus from form
        rollData.damageBonus = Number(form?.find('[name="damage-bonus"]').val()) + damageBonus;

        // Collect checked optional modifiers from form
        const usedOptionals = form
          ?.find(".modifier")
          ?.filter(function () {
            return $(this).prop("checked");
          })
          ?.map(function () {
            return Number($(this).prop("name").split(".")[1]);
          })
          .get()
          // Map checked optionals to different modifiers belonging to label
          .map((mod) => templateData.optionalModifiers?.[mod]);
        templateData.data.extraProps.push(
          ...(usedOptionals
            // @ts-expect-error unknown form, see Promise comment above
            ?.map((m) => m.name)
            .filter((o) => !templateData.data.extraProps.includes(o)) ?? [])
        );
        // Add optionals to roll formula
        parts.push(
          ...(usedOptionals?.flatMap(
            (optional) =>
              optional?.modifiers
                ?.filter((mod) => mod.type === "attack")
                ?.map((mod) => mod.formula) ?? []
          ) ?? [])
        );

        // Create and roll Roll, add render to data for chat message
        roll = await Roll.create(parts.join("+"), rollData).evaluate({ async: true });
        templateData.roll = await roll.render();

        // Add roll result and failure/success details
        templateData.result.type = localize(`${isCheck ? "NECH.Check" : "NECH.Attack"}`);
        if (roll.total != null && roll.total <= 1) {
          templateData.result.roll = localize("Roll.Fumble");
        } else if (roll.total && roll.total > 1 && roll.total <= 5) {
          templateData.result.roll = localize("Roll.Failure");
        } else if (roll.total && roll.total >= 6 && roll.total < 11) {
          templateData.result.roll = localize("Roll.Success");
          // Add damage/hit location according to roll total
          templateData.result.location =
            NECHRONICA.hitLocations[roll.total as keyof typeof NECHRONICA["hitLocations"]];
        } else if (roll.total && roll.total >= 11) {
          templateData.result.roll = localize("Roll.Critical");
          templateData.result.location = NECHRONICA.hitLocations.crit;
        }

        // Damage calculation
        if (isAttack && roll.total != null && roll.total >= 6) {
          damageParts.push(itemData.data.damage);
          if (roll.total > 10) damageParts.push(roll.total - 10);
          if (rollData.damageBonus) damageParts.push(rollData.damageBonus);
          // Add general modifier
          if (getProperty(rollData, `modifiers.${itemData.data.attack}.damage`)) {
            damageParts.push(`@modifiers.${itemData.data.attack}.damage`);
          }
          // Add optional modifier
          damageParts.push(
            ...(usedOptionals?.flatMap(
              (optional) =>
                optional?.modifiers
                  ?.filter((mod) => mod.type === "damage")
                  ?.map((mod) => mod.formula) ?? []
            ) ?? [])
          );

          // Create damage roll
          damageRoll = await Roll.create(damageParts.join("+"), rollData).roll({ async: true });
          templateData.damageRoll = await damageRoll.render();
          templateData.damage = {
            total: damageRoll.total ?? 0,
            formula: damageRoll.formula,
          };
        }
      } else {
        roll = new Roll("1").roll();
      }
    }

    // Use rollMode from form, fall back to undefined to let ChatMessage handle rollMode
    // @ts-expect-error See Promise comment above
    const rollMode = form?.find('[name="rollMode"]').val();
    // Define sound if no Dice So Nice is enabled
    const sound =
      getGame().dice3d?.enabled || rollType === NECHRONICA.RollType.MESSAGE
        ? undefined
        : CONFIG.sounds.dice;

    // Handle chat message
    const chatData = {
      user: getGame().user?.id,
      speaker: ChatMessage.getSpeaker({ actor: this.actor ?? undefined }),
      rollMode: rollMode,
      type:
        isCheck || isAttack
          ? foundry.CONST.CHAT_MESSAGE_TYPES.ROLL
          : foundry.CONST.CHAT_MESSAGE_TYPES.IC,
      roll: roll ?? undefined,
      content: await renderTemplate(template, templateData),
      flags: { roll, damageRoll },
      sound,
    };
    const message = await ChatMessage.create(chatData);

    // Return complete message for API usage
    return message;
  }
}

/**
 * An object containing additional chat data
 */
interface ItemChatData {
  effect: string;
  description: string;
  props: string[];
  extraProps: string[];
}

/* eslint-disable @typescript-eslint/no-explicit-any */
/*
 * An object containing all relevent information for rendering an item's roll or its dialogs.
 */
type TemplateData = {
  actor: NechronicaActor;
  tokenId?: string;
  item: NechronicaItemData;
  rollMode: keyof typeof CONFIG.Dice.rollModes;
  rollModes: typeof CONFIG.Dice.rollModes;
  config: typeof NECHRONICA;
  data: ItemChatData;
  isAttack: boolean;
  isCheck: boolean;
  hasAttack: boolean;
  spineCounter: number;
  roll?: string;
  optionalModifiers: { name: string; modifiers: Modifier[] }[] | undefined;
  dice: number;
  result: { roll: string; type: string; result: string; location: string };
  damageRoll?: string;
  damage?: { total: number | null; formula: string };
};
