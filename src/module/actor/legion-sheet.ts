import NechronicaActorSheet from "./actor-sheet";
export default class NechronicaActorSheetLegion extends NechronicaActorSheet {
  /** @override */
  static get defaultOptions(): BaseEntitySheet.Options {
    // @ts-expect-error mergeObject types
    return mergeObject(super.defaultOptions, {
      classes: [...super.defaultOptions.classes, "legion"],
    });
  }
}
