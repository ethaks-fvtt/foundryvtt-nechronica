/* eslint-disable */
// @ts-nocheck
import NechronicaActorSheet from "./actor-sheet";

/** @augments NechronicaActorSheet */
export default class NechronicaActorSheetDoll extends NechronicaActorSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      tabs: [
        {
          navSelector: ".sheet-tabs",
          contentSelector: ".sheet-body",
          initial: "parts",
        },
      ],
      classes: [...super.defaultOptions.classes, "doll"],
      scrollY: [".parts-body", ".biography.body"],
    });
  }

  constructor(...args) {
    super(...args);

    // Track display status of elements to persist after re-rendering
    this._hiddenElems = {};
  }

  /** @override */
  getData() {
    const data = super.getData();

    data.bonusAttribute =
      Object.entries(data.actor.data.attributes).find(([key, value]) => value.bonus)?.[0] ?? "";

    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Circle listener
    html.find(".circles").click((ev) => this._onCircleClick(ev));

    // Bonus Attribute choice
    html.find(".bonusAttribute>input").click(this._onBonusAttribute.bind(this));

    // html.find("a.hide-show").click(this._hideShowHeader.bind(this));
  }

  /**
   * Handle requests to set the floating bonus attribute point
   *
   * @param {Event} event        The triggering click event
   * @returns {Promise} update   The update diff
   * @private
   */
  _onBonusAttribute(event) {
    event.preventDefault();
    const actorData = duplicate(this.actor);
    const attribute = $(event.currentTarget).attr("value");
    for (const key of Object.keys(actorData.data.attributes)) {
      setProperty(actorData, `data.attributes.${key}.bonus`, key === attribute ? 1 : 0);
    }
    delete actorData.bonusAttribute;
    return this.actor.update(actorData);
  }

  /**
   * Handle requests involving circles to set a numeric value with a defined maximum
   *
   * @param {Event} event         The triggering click event
   * @returns {Promise} update    The update diff
   * @private
   */
  _onCircleClick(event) {
    const actorData = duplicate(this.actor);
    const index = Number($(event.currentTarget).attr("data-index"));
    let target = $(event.currentTarget).parents(".circle-row").attr("data-target");
    if (target === "item") {
      const itemData = duplicate(
        this.actor.items.find(
          (i) => i._id === $(event.currentTarget).parents(".item").attr("data-item-id")
        )
      );
      target = $(event.currentTarget).parents(".circle-row").attr("data-item-target");
      const value = getProperty(itemData, target);
      if (value === index + 1) {
        // If the last one was clicked, decrease by 1
        setProperty(itemData, target, index);
      } // Otherwise, value = index clicked
      else setProperty(itemData, target, index + 1);
      return this.actor.updateEmbeddedEntity("OwnedItem", itemData);
    }
    const value = getProperty(actorData, target);
    if (value === index + 1) {
      // If the last one was clicked, decrease by 1
      setProperty(actorData, target, index);
    } // Otherwise, value = index clicked
    else setProperty(actorData, target, index + 1);
    return this.actor.update(actorData);
  }

  /**
   * @override
   */
  _hideShowHeader(event) {
    event.preventDefault();
    const html = this._element;
    const header = html.find(".doll-header");
    const sheetBody = html.find(".sheet-body");

    if (header.hasClass("hidden")) {
      header.removeClass("hidden");

      sheetBody.removeClass("small-header");

      this._hiddenElems["doll-header"] = false;
    } else {
      header.addClass("hidden");
      sheetBody.addClass("small-header");

      this._hiddenElems["doll-header"] = true;
    }
  }
}
