import { NechronicaActorRollData } from "../actor/actor-prepared-data";
import { NechronicaItemRollData } from "../item/item-data";

/********************/
/* Utility types    */
/********************/

/** Either the complete type, or nothing from it */
export type AllOrNothing<T> = T | Partial<Record<keyof T, undefined>>;

// For convenience
type Primitive = string | number | bigint | boolean | undefined | symbol;

/**
 * A type including only valid property paths of a given type.
 * Paths are given in dot notations as `data.some.property`
 */
export type PropPath<T, Prefix = ""> = {
  [K in keyof T]: T[K] extends Primitive | Array<unknown>
    ? `${string & Prefix}${string & K}`
    : `${string & Prefix}${string & K}` | PropPath<T[K], `${string & Prefix}${string & K}.`>;
}[keyof T];

export type PropType<T, Path extends string> = string extends Path
  ? unknown
  : Path extends keyof T
  ? T[Path]
  : Path extends `${infer K}.${infer R}`
  ? K extends keyof T
    ? PropType<T[K], R>
    : unknown
  : unknown;

/** Recursively sets every property NonNullable */
export type DeepNonNullable<T> = {
  /* eslint-disable-next-line @typescript-eslint/ban-types */
  [P in keyof T]: T[P] extends object ? DeepNonNullable<T[P]> : NonNullable<T[P]>;
};

/** Drop first element of array */
/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export type DropFirst<T extends unknown[]> = T extends [any, ...infer U] ? U : never;

/********************/
/*  Common types    */
/********************/

/** Common rollData type */
export type NechronicaRollData = Record<string, unknown> &
  AllOrNothing<NechronicaActorRollData> &
  AllOrNothing<NechronicaItemRollData> & { rollBonus?: number; damageBonus?: number };
