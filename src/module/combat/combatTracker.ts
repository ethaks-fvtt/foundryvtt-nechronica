import NechronicaCombatantConfig from "./combatantConfig";

export default class NechronicaCombatTracker extends CombatTracker {
  _onConfigureCombatant(li: JQuery<HTMLElement>): void {
    const combatant = this.viewed?.combatants.get(li.data("combatant-id"));
    if (combatant)
      new NechronicaCombatantConfig(combatant, {
        top: Math.min(li[0].offsetTop, window.innerHeight - 350),
        left: window.innerWidth - 720,
        width: 400,
      }).render(true);
  }
}
