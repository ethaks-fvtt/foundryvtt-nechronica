import { NechronicaItemSheetData } from "../item/item-sheet";

interface NechronicaRollEditorOptions {
  hash: {
    button: boolean;
    content: string;
    editable: boolean;
    owner: boolean;
    target: string;
  };
  data: {
    root: NechronicaItemSheetData;
  };
}
export const registerHandlebarsHelpers: () => void = function () {
  /**
   * Creates an editor enriched with the actor's roll data
   *
   * @param options - Options to determine the editor's properties
   * @throws Undefined targets cause no editor data to be available
   * @returns HTML editor element
   */
  const nechRollEditor = (options: NechronicaRollEditorOptions) => {
    const rollData = options.data.root.document.getRollData();

    // Create editor
    const { target } = options.hash;
    if (!target) throw new Error("You must define the name of a target field.");

    // Enrich the content
    const owner = Boolean(options.hash.owner);
    const content = TextEditor.enrichHTML(options.hash.content || "", {
      secrets: owner,
      documents: true,
      rollData,
      rolls: true,
      links: true,
    });

    // Construct the HTML
    const editor = $(
      `<div class="editor"><div class="editor-content" data-edit="${target}">${content}</div></div>`
    );

    // Append edit button
    const button = Boolean(options.hash.button);
    const editable = Boolean(options.hash.editable);
    if (button && editable) {
      editor.append($('<a class="editor-edit"><i class="fas fa-edit"></i></a>'));
    }
    return new Handlebars.SafeString(editor[0].outerHTML);
  };

  /**
   * A helper to create a set of radio checkbox input elements in a named set.
   * The provided keys are the possible radio values while the provided values are human readable labels.
   *
   * @param name               The radio checkbox field name
   * @param choices            A mapping of radio checkbox values to human readable labels
   * @param options            Options
   * @param options.checked    Which key is currently checked?
   * @param options.localize  Pass each label through string localization?
   * @returns HTML radio checkbox input elements
   *
   * @example <caption>The provided input data</caption>
   * let groupName = "importantChoice";
   * let choices = {a: "Choice A", b: "Choice B"};
   * let chosen = "a";
   *
   * @example <caption>The template HTML structure</caption>
   * <div class="form-group">
   *   <label>Radio Group Label</label>
   *   <div class="form-fields">
   *     {{radioBoxes groupName choices checked=chosen localize=true}}
   *   </div>
   * </div>
   */
  const nechRadioBoxesRow = (
    name: string,
    choices: Record<string, string>,
    options: { hash: { checked: string } }
  ) => {
    const checked = options.hash.checked || null;
    let html = "";
    for (const key of Object.keys(choices)) {
      const isChecked = checked === key;
      html += `<label class="checkbox ${name}"><input type="radio" value="${key}" ${
        isChecked ? "checked" : ""
      }></label>`;
    }
    return new Handlebars.SafeString(html);
  };

  /**
   * A helper to assign and <input> within its block as checked based on its value
   * Escape the string as handlebars would, then escape any regexp characters in it
   *
   * @param value -             Value to check
   * @param options -           Options
   * @returns HTML with correctly checked input
   */
  /* eslint-disable-next-line @typescript-eslint/ban-types */
  const nechRadioCheck = (value: string, options: object): Handlebars.SafeString => {
    const escapedValue = RegExp.escape(Handlebars.escapeExpression(value));
    const rgx = new RegExp(" value=[\"']" + escapedValue + "[\"']");
    // @ts-expect-error Handlebars type depths
    const html = options.fn(this);
    return html.replace(rgx, "$& checked");
  };

  Handlebars.registerHelper({
    nechRollEditor,
    nechRadioCheck,
    nechRadioBoxesRow,
  });
};
